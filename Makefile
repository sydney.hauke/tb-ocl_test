BUILD_DIR=build
SRC_DIR=src

CC=gcc

CFLAGS= -std=c11 -Wall -Wextra -g
LDFLAGS= -lOpenCL

BIN=vecadd

SRC=$(wildcard $(SRC_DIR)/*.c)
OBJ=$(SRC:.c=.o)

OCL_ICD_VENDORS=/usr/local/etc/OpenCL/vendors

.phony: clean build_dir run run_debug

all: build_dir $(BIN)

run: all
	export OCL_ICD_VENDORS=$(OCL_ICD_VENDORS); \
	export POCL_DEBUG=0; \
	export POCL_LEAVE_KERNEL_COMPILER_TEMP_FILES=0; \
	./$(BUILD_DIR)/$(BIN)

run_debug: all
	export OCL_ICD_VENDORS=$(OCL_ICD_VENDORS); \
	export POCL_DEBUG=all; \
	export POCL_LEAVE_KERNEL_COMPILER_TEMP_FILES=1; \
	./$(BUILD_DIR)/$(BIN)

build_dir: $(BUILD_DIR)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BIN): $(OBJ)
	$(CC) -o $(BUILD_DIR)/$@ $< $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(SRC_DIR)/*.o
