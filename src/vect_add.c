#include <stdio.h>
#include <stdlib.h>

/* Link against the OpenCL library. Normally, an ICD loader
 * stands behind this library and proxies all calls to a
 * choosen OpenCL platform installed on the host.
 **/
#include <CL/cl.h>

/* OpenCL C kernel "vecadd".
 * Given two vectors A and B, compute their vector addition
 * and store the result in vector C
 */
const char *programSource =
    "__kernel                                         \n"
    "void vecadd(__global int *A,                     \n"
    "            __global int *B,                     \n"
    "            __global int *C)                     \n"
    "{                                                \n"
    "                                                 \n"
    "   int idx = get_global_id(0);                   \n"
    "                                                 \n"
    "   C[idx] = A[idx] + B[idx];                     \n"
    "}                                                \n";

int main(void)
{
  int *A = NULL;
  int *B = NULL;
  int *C = NULL;

  const int elements = 1000000;

  size_t datasize = sizeof(int) * elements;

  A = (int *)malloc(datasize);
  B = (int *)malloc(datasize);
  C = (int *)malloc(datasize);

  /* Assign a linear sequence of numbers to both A and B */
  for (int i = 0; i < elements; i++)
  {
    A[i] = i;
    B[i] = i;
  }

  /* === Discover and initialize the platform(s) === */
  cl_int status;

  cl_uint num_platforms = 0;
  cl_platform_id *platforms = NULL;

  clGetPlatformIDs(0, NULL, &num_platforms); // Discover number of platforms

  platforms = (cl_platform_id *)malloc(num_platforms * sizeof(cl_platform_id));
  if(platforms == NULL) {
    perror("platforms malloc");
    return EXIT_FAILURE;
  }

  clGetPlatformIDs(num_platforms, platforms, NULL); // Initialize platform infos

  printf("%u platform(s) detected :\n", num_platforms);

  if(num_platforms == 0) {
    printf("No platform detected. Exiting...\n");
    return EXIT_SUCCESS;
  }

  /* Print all available platforms */
  for (size_t i = 0; i < num_platforms; i++) {
    char *platform_string;
    size_t buf_size;

    clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 0, NULL, &buf_size);
    platform_string = malloc(buf_size); // Very nasty...
    clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, buf_size, platform_string, NULL);

    printf("\t#%lu : %s\n", i, platform_string);
    free(platform_string); // Very nasty...
  }

  /* Prompt the user to choose one of the platforms discovered */
  size_t platform;
  while(1) {
    printf("Choose platform [0-%u] : ", num_platforms-1);
    fflush(stdin);
    int ret = fscanf(stdin, "%lu", &platform);
    if(ret == 1 && platform < num_platforms) break;
  }

  /* === Discover and initialize the devices === */
  cl_uint numDevices = 0;
  cl_device_id *devices = NULL;

  clGetDeviceIDs(platforms[platform], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
  devices = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));
  if(devices == NULL) {
    perror("devices malloc");
    return EXIT_FAILURE;
  }
  clGetDeviceIDs(platforms[platform], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);

  printf("%u devices found : \n", numDevices);

  /* Print all available devices for the choosen platform */
  for(size_t i = 0; i < numDevices; i++) {
    char *device_name;
    size_t buf_size;

    clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 0, NULL, &buf_size);
    device_name = malloc(buf_size);
    clGetDeviceInfo(devices[i], CL_DEVICE_NAME, CL_DEVICE_NAME, device_name, NULL);

    printf("\t#%lu : %s\n", i, device_name);
    free(device_name);
  }

  /* Prompt the user to choose one of the available devices */
  size_t device;
  while(1) {
    printf("Choose device [0-%u] : ", numDevices-1);
    fflush(stdin);
    int ret = fscanf(stdin, "%lu", &device);
    if(ret == 1 && device < numDevices) break;
  }

  /* === Create a context for the choosen device === */
  cl_context context = NULL;

  context = clCreateContext(NULL, 1, &devices[device], NULL, NULL, &status);

  /* === Create a command queue for the choosen device === */
  cl_command_queue cmdQueue;

  cmdQueue = clCreateCommandQueue(context, devices[device], 0, &status);

  /* === Create device buffers in our context === */
  cl_mem bufferA;
  cl_mem bufferB;
  cl_mem bufferC;

  bufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL, &status);
  bufferB = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL, &status);
  bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, datasize, NULL, &status);

  /* === Write host data to device buffers === */
  status = clEnqueueWriteBuffer(cmdQueue,
                                bufferA,
                                CL_FALSE,
                                0,
                                datasize,
                                A,
                                0,
                                NULL,
                                NULL);

  status = clEnqueueWriteBuffer(cmdQueue,
                                bufferB,
                                CL_FALSE,
                                0,
                                datasize,
                                B,
                                0,
                                NULL,
                                NULL);

  /* === Create and initialize the program data structure */
  cl_program program = clCreateProgramWithSource(context,
                                                 1,
                                                 (const char **)&programSource,
                                                 NULL,
                                                 &status);

  /* === Compile the program === */
  status = clBuildProgram(program,
                          0,
                          NULL,
                          NULL,
                          NULL,
                          NULL);

  /* === Create the "kernel" based on our program (which we
   * actually refer to the kernel) === */
  cl_kernel kernel = NULL;

  kernel = clCreateKernel(program, "vecadd", &status);

  /* === Set the kernel arguments === */
  status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufferA);
  status |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufferB);
  status |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufferC);

  /* === Configure the work-item structure === */
  size_t globalWorkSize[1];
  globalWorkSize[0] = elements;

  /* === Enqueue the kernel for execution === */
  status = clEnqueueNDRangeKernel(cmdQueue,
                                  kernel,
                                  1,
                                  NULL,
                                  globalWorkSize,
                                  NULL,
                                  0,
                                  NULL,
                                  NULL);

  /* === Read the output buffer back to the host === */
  clEnqueueReadBuffer(cmdQueue,
                      bufferC,
                      CL_TRUE,
                      0,
                      datasize,
                      C,
                      0,
                      NULL,
                      NULL);

  /* Verify the output */
  int result = 1;
  for (int i = 0; i < elements; i++)
  {
    if (C[i] != 2 * i)
    {
      result = 0;
      break;
    }
  }

  if (result) {
    printf("Output is correct\n");
  }
  else {
    printf("Output is incorrect\n");
  }

  /* Free OpenCL resources */
  clReleaseKernel(kernel);
  clReleaseProgram(program);
  clReleaseCommandQueue(cmdQueue);
  clReleaseMemObject(bufferA);
  clReleaseMemObject(bufferB);
  clReleaseMemObject(bufferC);
  clReleaseContext(context);

  /* Free host resources */
  free(A);
  free(B);
  free(C);
  free(platforms);
  free(devices);

  return EXIT_SUCCESS;
}
